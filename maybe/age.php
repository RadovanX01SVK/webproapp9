<!DOCTYPE html>
<html>
<body>

<h2>My First JavaScript</h2>

<script>
  function getAge(dateString) {
      var today = new Date();
      var birthDate = new Date(dateString);
      var age = today.getFullYear() - birthDate.getFullYear();
      var m = today.getMonth() - birthDate.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
          age--;
      }
      return age;
  }

</script>

<button type="button" onclick="getAge(210317)">Try it</button>

</body>
</html>
