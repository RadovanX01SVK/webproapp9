<?xml version="1.0" encoding="UTF-8"?>
  <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
    <body>
      <h2>List of Goblins</h2>
      <table border="1">
        <tr bgcolor="#42bcf4">
          <th style="text-align:left">name</th>
          <th style="text-align:left">father</th>
          <th style="text-align:left">clan</th>
          <th style="text-align:left">mother</th>
          <th style="text-align:left">class</th>
          <th style="text-align:left">died</th>
        </tr>
        <xsl:for-each select="goblins/entry">
        <tr>
          <td><xsl:value-of select="name"/></td>
          <td><xsl:value-of select="father"/></td>
          <td><xsl:value-of select="clan"/></td>
          <td><xsl:value-of select="mother"/></td>
          <td><xsl:value-of select="class"/></td>
          <td><xsl:value-of select="died"/></td>
        </tr>
        </xsl:for-each>
      </table>
    </body>
    </html>
    </xsl:template>
    </xsl:stylesheet>
