<!DOCTYPE html>
<html>
	<head>
		<title>Goblin's Book</title>
		<link href="/app9/css/goblin.css" rel="stylesheet" type="text/css"/>
	</head>



<body>


	<div id="main">
		<header id="header">
			<a href="/app9/partials/main.php">
				<img src="/app9/img/gobLogo01.png" alt="Logo" id="logo"/>
			</a>

		</header>

		<div class="nav">
			<?php include '../partials/nav.php'; ?>
	 	</div>

		<div class="center">
			<h3>Here you can see registered Goblins</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>


		<div class="center">
			<?php
				// edited - original taken from w3school..
				echo "<table style='border: solid 1px black;'>";
				echo "<tr><th>Id</th><th>Firstname</th><th>Lastname</th><th>PhoneNumber</th></tr>";

				class TableRows extends RecursiveIteratorIterator {
				    function __construct($it) {
				        parent::__construct($it, self::LEAVES_ONLY);
				    }

				    function current() {
				        return "<td style='width:150px;border:1px solid black;'>" . parent::current(). "</td>";
				    }

				    function beginChildren() {
				        echo "<tr>";
				    }

				    function endChildren() {
				        echo "</tr>" . "\n";
				    }
				}

				$servername = "localhost";
				$username = "root";
				$password = "Qwert12345";
				$dbname = "GOBLINSDB01";

				try {
				    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
				    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				    $stmt = $conn->prepare("SELECT GoblinID, FirstName, LastName, PhoneNumber FROM Goblins");
				    $stmt->execute();

				    // set the resulting array to associative
				    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
				    foreach(new TableRows(new RecursiveArrayIterator($stmt->fetchAll())) as $k=>$v) {
				        echo $v;
				    }
				}
				catch(PDOException $e) {
				    echo "Error: " . $e->getMessage();
				}
				$conn = null;
				echo "</table>";
				?>
			</div>

</body>
</html>
